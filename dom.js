var itemTable = document.getElementById('items');
var newItem = document.getElementById('item');
var previous = document.getElementById('previous');
var next = document.getElementById('next');
var sortingInfo = document.getElementById('sortingInfo');
let currentPage = 0;
let sortUp = false;

let dataCollection = [
    {name:"Lamborghini Urus", price:"310000", image:"../img/urus.png"},
    {name:"Lamborghini Aventador", price:"580000", image:"../img/aventador.png"},
    {name:"Lamborghini Countach", price:"2300000", image:"../img/countach.png"},
    {name:"Lotus Evija", price:"1900000", image:"../img/evija.png"},
    {name:"Spyker c8 Preliator", price:"315000", image:"../img/preliator.png"},
    {name:"Aston Martin Vantage", price:"203000", image:"../img/vantage.png"},
    {name:"Lamborghini Huracán", price:"318000", image:"../img/huracan.png"},
    {name:"Lamborghini Sián 2020", price:"3335000", image:"../img/sian.png"},
    {name:"Aston Martin Valkyrie", price:"279000", image:"../img/valkyrie.png"},
    {name:"Mclaren 720s", price:"307000", image:"../img/720s.png"},
    {name:"Mclaren Senna Gtr", price:"1700000", image:"../img/sennagtr.png"},
    {name:"Ferrari Testarossa", price:"105000", image:"../img/testarossa.png"},
    {name:"Ferrari SF90", price:"453000", image:"../img/sf90.png"}
];
console.log(dataCollection);

previous.addEventListener('click', loadPrevious);
next.addEventListener('click', loadNext);


const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

const comparer = (idx, asc) => (a, b) => ((v1, v2) => 
    v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
    )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));

// do the work...
document.querySelectorAll('th').forEach(th => th.addEventListener('click', (() => {
    const table = th.closest('table');
    sort();
    Array.from(table.querySelectorAll('tr:nth-child(n+2)'))
        .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
        .forEach(tr => table.appendChild(tr) );
})));

function sort() {
    if(sortUp === false) {
        sortingInfo.innerHTML = "Prijs sortering: Oplopend";
        sortUp = true;
    }
    else {
        sortingInfo.innerHTML = "Prijs sortering: Aflopend"
        sortUp = false;
    }


}

function loadPrevious() {
    if(currentPage === 2){
        //Go to page 1
        LoadPage1();
        currentPage = 1;
    }
    else if(currentPage === 1){
        //Go to page 0
        LoadPage0();
        currentPage = 0;
    }
    else if(currentPage === 0){
        //Do nothing
    }
}

function loadNext() {
    if(currentPage === 2){
        //Do nothing
    }
    else if(currentPage === 1){
        //Go to page 2
        LoadPage2();
        currentPage = 2;
    }
    else if(currentPage === 0){
        //Go to page 1
        LoadPage1();
        currentPage = 1;
    }
    console.log(itemTable.children[0].childElementCount);
}

function LoadPage0(){
    deleteExistingRows();

    for (let i = 0; i < 5; i++) {
        var row = itemTable.insertRow(1);
    
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
    
        cell1.innerHTML = dataCollection[i].name;
        cell2.innerHTML = dataCollection[i].price;
        var image = document.createElement("img");
            image.src = dataCollection[i].image;
        cell3.appendChild(image);
    }
}

function LoadPage1(){
    clearPriceTag();
    deleteExistingRows();

    for (let i = 5; i < 10; i++) {
        var row = itemTable.insertRow(1);
    
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
    
        cell1.innerHTML = dataCollection[i].name;
        cell2.innerHTML = dataCollection[i].price;
        var image = document.createElement("img");
            image.src= dataCollection[i].image;
        cell3.appendChild(image);
    }
}

function LoadPage2(){
    clearPriceTag();
    deleteExistingRows();

    for (let i = 10; i < 13; i++) {
        var row = itemTable.insertRow(1);
    
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
    
        cell1.innerHTML = dataCollection[i].name;
        cell2.innerHTML = dataCollection[i].price;
        var image = document.createElement("img");
            image.src= dataCollection[i].image;
        cell3.appendChild(image);
    }
}

function clearPriceTag() {
    sortingInfo.innerHTML = "Prijs sortering: geen";
}

function deleteExistingRows(){
    if(currentPage !== 2) {
        for (let i = 0; i < 5; i++) {
            itemTable.deleteRow(1);
        }
    }
    else
    {
        for (let i = 0; i < 3; i++) {
            itemTable.deleteRow(1);
        }
    }
}

for (let i = 0; i < 5; i++) {
    var row = itemTable.insertRow(1);

    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);

    cell1.innerHTML = dataCollection[i].name;
    cell2.innerHTML = dataCollection[i].price;
    var image = document.createElement("img");
        image.src= dataCollection[i].image;
    cell3.appendChild(image);
}

console.log(itemTable);
console.log(itemTable.children[0].childElementCount);