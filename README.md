# FrontendBobbyWeek1
Dit is mijn uitwerking van oefensprint 1 week 1:<br/>
Hierbij heb ik gebruik gemaakt van HTML/CSS/JS<br/>

## URL
https://thuas.gitlab.io/dt-webapplications/wa-2122-s4/intervisiegroepen/groep-7/frontendbobbyweek1

## Credits
CSS Crash Course For Absolute Beginners - Traversy Media (Youtube)<br/>
JavaScript DOM Crash Course - Part 1 / Part 2 / Part 3 / Part 4 - Traversy Media (Youtube)<br/>
Table insertRow() Method - https://www.w3schools.com/jsref/met_table_insertrow.asp <br/>
variable image binding to HTML Table - https://stackoverflow.com/questions/18362791/im-trying-to-add-an-image-to-a-dynamic-table-row-and-have-no-idea-how-to-accomp <br/>
table sorting - https://stackoverflow.com/questions/14267781/sorting-html-table-with-javascript <br/>

## Versies
0.1  Toegevoegd: HTML basis<br/>
0.2  Toegevoegd: CSS en pipeline CI/C<br/>
0.21 Toegevoegd: Gitlab Pages Screenshot<br/>
0.3  Toegevoegd: Javascript<br/>

## Belangrijk
Ik heb eerst in een andere Repo gewerkt, niet wetende dat dit in de intervisiegroep->personal repo moest.<br/>
Bijgevoegd een image met de commits uit de vorige repo als commits.png.<br/>